# README

# Make a virtualenv

`python -m venv env`

# activate your venv

`source env/bin/activate`

# Install dependencies

`pip install -r requirements.txt`

# Phase 1

`cd mercadolibre`

`scrapy crawl catalogos`

# Phase 2

`python phase2/detail-product.py`

# Phase 3

`python phase3/detail-vendor.py`

# Output

`mercadolibre/data.csv`
