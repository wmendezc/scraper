from tempfile import NamedTemporaryFile
from requests import session
from bs4 import BeautifulSoup as bs
import shutil
import csv
import os


def make_request(link):
    request = session()
    response = request.get(link)
    return (response.text)


def parse_categories(data):
    parsed_data = bs(data, 'html.parser')
    items = parsed_data.find_all('a', 'breadcrumb')
    collect = []
    for k, item in enumerate(items):
        jsondata = {}
        jsondata[f'categoria_{k + 1}'] = str(item.text).strip('\t\n')
        collect.append(jsondata)
    return collect


def parse_vendor_link(data):
    parsed_data = bs(data, 'html.parser')
    items = parsed_data.find_all('a', 'reputation-view-more')
    #import ipdb
    # ipdb.set_trace()
    jsondata = {}
    for item in items:
        jsondata[f'vendor_link'] = str(item['href'])
    return jsondata


if __name__ == '__main__':
    # Open CSV File
    headers = ['id', 'product', 'product_link', 'categoria_1',
               'categoria_2', 'categoria_3', 'vendor_link',
               'vendor_name', 'vendor_sales', 'vendor_location']
    tempfile = NamedTemporaryFile(mode='w', delete=False)
    #filename = '../data.csv'
    fileDir = os.path.dirname(os.path.realpath('__file__'))
    filename = os.path.join(fileDir, 'data.csv')
    filename = os.path.abspath(os.path.realpath(filename))

    with open(filename, newline='') as csvfile, tempfile:
        reader = csv.DictReader(csvfile)
        writer = csv.DictWriter(tempfile, headers)
        writer.writeheader()

        for row in reader:
            if row:
                data = make_request(row['product_link'])
                categories = parse_categories(data)
                vendor_link = parse_vendor_link(data)
                row['categoria_1'] = categories[0]['categoria_1']
                row['categoria_2'] = categories[1]['categoria_2']
                if len(categories) > 2:
                    row['categoria_3'] = categories[2]['categoria_3']
                print(f"{row['id']} {row['product_link']}")
                print(vendor_link)
                row['vendor_link'] = vendor_link.get('vendor_link')
                row['vendor_name'] = ''
                row['vendor_sales'] = ''
                row['vendor_location'] = ''
            writer.writerow(row)

    shutil.move(tempfile.name, filename)
    print("Finished!")
