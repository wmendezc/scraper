from tempfile import NamedTemporaryFile
from requests import session
from bs4 import BeautifulSoup as bs
import shutil
import csv
import os


def make_request(link):
    request = session()
    response = request.get(link)
    return (response.text)


def parse_vendor(data):
    parsed_data = bs(data, 'html.parser')
    vendor_name = parsed_data.find('h3', 'store-info__name')
    if not vendor_name:
        vendor_name = parsed_data.find('h3', 'brand')
        if not vendor_name:
            return "Perfil inaccesible"
    # print(str(vendor_name.text).strip('\t\n'))
    return str(vendor_name.text).strip('\t\n')


def parse_vendor_sales(data):
    parsed_data = bs(data, 'html.parser')
    vendor_sales = parsed_data.find('p', 'seller-info__subtitle-sales')
    if not vendor_sales:
        return "Este vendedor aún no tiene suficientes ventas para calcular su reputación"
    return str(vendor_sales.text).strip('\t\n')


def parse_vendor_location(data):
    parsed_data = bs(data, 'html.parser')
    vendor_location = parsed_data.find('p', 'location-subtitle')
    if not vendor_location:
        return 'Sin ubicación en la página de perfil'
    return str(vendor_location.text).strip('\t\n')


if __name__ == '__main__':
    # Open CSV File
    headers = ['id', 'product', 'product_link', 'categoria_1',
               'categoria_2', 'categoria_3', 'vendor_link', 'vendor_name',
               'vendor_sales', 'vendor_location']
    tempfile = NamedTemporaryFile(mode='w', delete=False)
    fileDir = os.path.dirname(os.path.realpath('__file__'))
    filename = os.path.join(fileDir, 'data.csv')
    filename = os.path.abspath(os.path.realpath(filename))

    with open(filename, newline='') as csvfile, tempfile:
        reader = csv.DictReader(csvfile)
        writer = csv.DictWriter(tempfile, headers)
        writer.writeheader()

        for row in reader:
            if row['vendor_link']:
                data = make_request(row['vendor_link'])
                vendor_name = parse_vendor(data)
                vendor_sales = parse_vendor_sales(data)
                vendor_location = parse_vendor_location(data)
                row['vendor_name'] = vendor_name
                row['vendor_sales'] = vendor_sales
                row['vendor_location'] = vendor_location
                print(row)
            writer.writerow(row)

    shutil.move(tempfile.name, filename)
    print("DetailVendor Done!")
